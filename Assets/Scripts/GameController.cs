﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    static GameController gameController;
    public User chosenUser;

	void Awake () {

        if (gameController == null)
        {
            DontDestroyOnLoad(gameObject);
            gameController = this;
        }
        else
        {
            Destroy(gameObject);
        }

	}
    public static GameController GetInstance()
    {
        return gameController;
    }

    public void CreateUser(string _login, string _pass, string rtext)
    {

        chosenUser = new User();
        chosenUser.login = _login;
        chosenUser.password = _pass;
        chosenUser.id = int.Parse(rtext.Split('>')[1]);
    }
    public void SetActiveObjects(string tag,bool setActive)
    {
       GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);
       foreach (GameObject obj in objs)
       {
           obj.SetActive(setActive);

       }
    }
	
}
