﻿using UnityEngine;
using System.Collections;

public class Ping : MonoBehaviour
{
		int buttonWidth = 100, buttonHeight = 50, windowWidth = 300, windowHeight = 200;
		public bool is_active = false;
		public string fromWhomName, message;
		public int idEvent, fromWhomId;
		public GUISkin generalSkin;
		FightScript fightScript;
		public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/Ping.ashx";
		// Use this for initialization
		void Start ()
		{
	
		}
	
		public IEnumerator PingCall (string _actor, string _victim, string _message, int _stage, int _id, int _result)
		{
				WWWForm form = new WWWForm ();
				form.AddField ("actor", _actor);
				form.AddField ("victim", _victim);
				form.AddField ("message", _message);
				form.AddField ("stage", _stage);
				form.AddField ("id", _id);
				form.AddField ("result", _result);
				var r = new WWW (PHPUrl, form);
				yield return r;
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
				} else {
						Debug.Log ("Success");
						string result = r.text.Split ('<') [0];
						if (result [0] == 'y') {
								Debug.Log ("Запрос прошел и записан в базу");
						} else {
								Debug.Log ("Запрос добавить в базу не удалось");
						}
				}
		
		}

		public void Activate ()
		{
				is_active = true;

		}

		public void Deactivate ()
		{
				is_active = false;
		}

		void Awake ()
		{
				fightScript = GameObject.FindGameObjectWithTag ("FightManager").GetComponent<FightScript> ();

		}

		void MessageBoxWindow (int id)
		{
				GUI.Label (new Rect (10, 10, 280, 100), message);
				if (GUI.Button (new Rect (windowWidth / 2 - buttonWidth * 3 / 2, windowHeight / 2, buttonWidth, buttonHeight), "Да")) {
						Deactivate ();
						StartCoroutine (PingCall (fightScript.GetCharID (SecondSceneGUI.chosenCharacter), fromWhomId.ToString (), "", 2, idEvent, 1));
						
				}
				if (GUI.Button (new Rect (windowWidth / 2 + buttonWidth / 2, windowHeight / 2, buttonWidth, buttonHeight), "Нет")) {
						Deactivate ();
						StartCoroutine (PingCall (fightScript.GetCharID (SecondSceneGUI.chosenCharacter), fromWhomId.ToString (), "Я Вас не вижуи сплю, так что идите вхуй!", 2, idEvent, 0));
						
				}
		}

		void OnGUI ()
		{
				if (is_active) {
						GUI.skin = generalSkin;
						GUI.ModalWindow (12, new Rect (Screen.width / 2 - windowWidth / 2, Screen.height / 2 - windowHeight / 2, windowWidth, windowHeight), MessageBoxWindow, "Вас пингуют!");
				}
		}
}
