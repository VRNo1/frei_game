﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoginScript : MonoBehaviour
{
    public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/login2.ashx"; // адрес скрипта
    public Text loginText;
    public Text passText;
    public Text mailText;
    public GameObject mailLabel;
    public GameObject inputMail;
    public Button enterButton;
    public Button regButton;
    public Button regDoneButton;

    Showmessage showmessage;

    void Awake()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        showmessage = GetComponent<Showmessage>();

    }
    /// <summary>
    /// Процедура вызова веб-сервиса для регистрации
    /// </summary>
    /// <param name="_login">логин</param>
    /// <param name="_pass">пароль</param>
    /// <param name="_mail">почта</param>
    /// <returns></returns>
    IEnumerator Register(string _login, string _pass, string _mail)
    {
        WWWForm form = new WWWForm();
     
        form.AddField("login", _login);
        form.AddField("password", _pass);
        form.AddField("mail", _mail);
        form.AddField("code", "mail");
        var r = new WWW(PHPUrl, form);
        yield return r;
        if (r.error != null)
        {
            Debug.Log(r.error);
            showmessage.ShowMessage("Сервер не отвечает!");
        }
        else
        {

            Debug.Log("Success");
            string result = r.text.Split('<')[0];
            Debug.Log(r.text);
            switch (result[0])
            {
                case 'm':
                    showmessage.ShowMessage("Такая почта уже зарегестрирована!");
                    break;
                case 'l':
                    showmessage.ShowMessage("Такой логин уже используется!");
                    break;
                case 'y':
                    ChangeToLogin();
                    break;
                default:
                    break;
            }
        }

    }
    /// <summary>
    /// Процедура вызова веб-сервиса для логина
    /// </summary>
    /// <param name="_login">логин</param>
    /// <param name="_pass">пароль</param>
    /// <returns></returns>
    IEnumerator Login(string _login, string _pass)
    {
        Debug.Log("Login");
        WWWForm form = new WWWForm();
        form.AddField("login", _login);
        form.AddField("password", _pass);
        form.AddField("code", "login");
        var r = new WWW(PHPUrl, form);
        yield return r;
        if (r.error != null)
        {
            Debug.Log("Fail");
            Debug.Log(r.error);
            showmessage.ShowMessage("Сервер не отвечает!");
        }
        else
        {
            Debug.Log("Success");
            string result = r.text.Split('>')[0];
            if (result[0] == 'y')
            {
                GameController.GetInstance().CreateUser(_login, _pass, r.text);
                Application.LoadLevel("SecondScene");
            }
            else
            {
                showmessage.ShowMessage("Неправильный логин или пароль!");
            }
            Debug.Log(r.text);
        }

    }

    /// <summary>
    /// Клик на логин
    /// </summary>
    public void Login_Click()
    {
        StartCoroutine(Login(loginText.text, passText.text)); ;
    }

    /// <summary>
    /// Клик по кнопке смены окна на регистрационное
    /// </summary>
    public void Registration_Click()
    {
        ChangeToRegister();
    }

    /// <summary>
    /// Клик по кнопке регистрируещей новый логин
    /// </summary>
    public void RegistrationDone_Click()
    {
        StartCoroutine(Register(loginText.text, passText.text,mailText.text)); 
    }

    /// <summary>
    /// Меняет интерфейс на окно регистрации
    /// </summary>
    void ChangeToRegister()
    {
        mailLabel.SetActive(true);
        inputMail.SetActive(true);
        enterButton.gameObject.SetActive(false);
        regButton.gameObject.SetActive(false);
        regDoneButton.gameObject.SetActive(true);
    }
    /// <summary>
    /// Меняет интерфейс на окно Логина
    /// </summary>
    void ChangeToLogin()
    {
        mailLabel.SetActive(false);
        inputMail.SetActive(false);
        enterButton.gameObject.SetActive(true);
        regButton.gameObject.SetActive(true);
        regDoneButton.gameObject.SetActive(false);
    }
}
