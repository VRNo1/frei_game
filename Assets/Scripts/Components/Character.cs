﻿using UnityEngine;
using System.Collections;

public class Character
{

		public string charName;
		public int id;

		public Character (int _id, string _name)
		{
				id = _id;
				charName = _name;
		}

		public Character ()
		{
		
		}
}
