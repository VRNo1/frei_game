﻿using UnityEngine;
using System.Collections;

public class AlchemyGUI : MonoBehaviour {
	private string[] toolbarStringsReagent = {"Пропитка", "Лакировка", "Шлифование"};
	private string[] toolbarStringsPotion = {"Лечение", "Мана", "Восстановление"};
	private string[] toolbarStringsStone = {"Прилива", "Хаоса"};
	private string[] toolbarStringsFirst = {"Зелья", "Реагенты", "Камни"};
	private string[] toolbarStringsPotionGrade = {"Малое", "Среднее", "Большое","Хрустальное","Драгоценное"};
	private string[] toolbarStringsReagentGrade = {"Густой", "Очищенный", "Процеженный","Отфильтрованный"};
	private string[] toolbarStringsStoneGrade = {"Хуевый","Заебатый"};
	private string[] toolbarStringsSecond={}, toolbarStringsThird={};
	void Awake()

	{
		Screen.SetResolution (720, 1280, true);
		toolbarStringsSecond = toolbarStringsPotion;
		toolbarStringsThird = toolbarStringsPotionGrade;
		Debug.Log ("ONAwake");
	}
	private int toolbarFirstInt = 0,toolbarSecondInt=0,toolbarThirdInt=0;
	bool flagCount=false;
	string argum1="",argum3="", argum4="",argum5="";
	string argum2="";
	string result;
	string argumLabel1,argumLabel2,argumLabel3,argumLabel4,argumLabel5;

	int lengthToolButton=180,widthToolButton=50,margin=25,lengthTextField=90,heightTextField=40,lengthLabel=350,buttonLength=100,buttonHeight=60;

	void OnGUI ()
	{
		GUI.skin.label.fontSize = 30;
		GUI.skin.textField.fontSize = 30;
		toolbarFirstInt = GUI.SelectionGrid(new Rect (Screen.width/6-lengthToolButton/2, margin, lengthToolButton, widthToolButton*toolbarStringsFirst.Length), toolbarFirstInt, toolbarStringsFirst,1);
		switch (toolbarFirstInt) {
				case 0:
						toolbarStringsSecond = toolbarStringsPotion;
						toolbarStringsThird = toolbarStringsPotionGrade;
						break;
				case 1:
						toolbarStringsSecond = toolbarStringsReagent;
						toolbarStringsThird = toolbarStringsReagentGrade;
						break;
				case 2:
						toolbarStringsSecond = toolbarStringsStone;
						toolbarStringsThird = toolbarStringsStoneGrade;
						break;
				default:
						break;
				}
		toolbarSecondInt = GUI.SelectionGrid(new Rect (Screen.width/2-lengthToolButton/2, margin, lengthToolButton, widthToolButton*toolbarStringsSecond.Length), toolbarSecondInt, toolbarStringsSecond,1);
		toolbarThirdInt = GUI.SelectionGrid(new Rect (Screen.width/6*5-lengthToolButton/2, margin, lengthToolButton, widthToolButton*toolbarStringsThird.Length), toolbarThirdInt, toolbarStringsThird,1);
		int switch10 = toolbarThirdInt + toolbarSecondInt * 10 + toolbarFirstInt * 100;
    	switch (switch10) {
				case 100:
						argumLabel1 = "Клевер(100)";
						argumLabel2 = "Розы(100)";
						break;
				case 110:
						argumLabel1 = "Рис(100)";
						argumLabel2 = "Кукуруза(100)";
						break;
				case 120:
						argumLabel1 = "Азалия(100)";
						argumLabel2 = "Нарциссы(100)";
						break;
				case 4:
					argumLabel1 = "Константа";
					argum1="205";
					argumLabel2 = "Кордицепс(100)";
					argumLabel3 = "Лотос(100)";
					argumLabel4 = "Пыль звездного(10)";
					argumLabel5 = "Пыль дельфийская(10)";
					break;
				case 14:
					argumLabel1 = "Константа";
					argum1="205";
					argumLabel2 = "Сломанный рог(10)";
					argumLabel3 = "Кактус(100)";
					argumLabel4 = "Пыль звездного(10)";
					argumLabel5 = "Пыль дельфийская(10)";
					break;
				default:
						break;
				}
		argum1 = GUI.TextField (new Rect (margin, Screen.height/2+margin, lengthTextField, heightTextField), argum1);
		argum2 = GUI.TextField (new Rect (margin, Screen.height/2+margin*2+heightTextField, lengthTextField, heightTextField), argum2);
		argum3 = GUI.TextField (new Rect (margin, Screen.height/2+margin*3+heightTextField*2, lengthTextField, heightTextField), argum3);
		argum4 = GUI.TextField (new Rect (margin, Screen.height/2+margin*4+heightTextField*3, lengthTextField, heightTextField), argum4);
		argum5 = GUI.TextField (new Rect (margin, Screen.height/2+margin*5+heightTextField*4, lengthTextField, heightTextField), argum5);
		GUI.Label(new Rect(margin*2+lengthTextField, Screen.height/2+margin, lengthLabel, heightTextField),argumLabel1);
		GUI.Label(new Rect(margin*2+lengthTextField, Screen.height/2+margin*2+heightTextField, lengthLabel, heightTextField),argumLabel2);
		GUI.Label(new Rect(margin*2+lengthTextField, Screen.height/2+margin*3+heightTextField*2, lengthLabel, heightTextField),argumLabel3);
		GUI.Label(new Rect(margin*2+lengthTextField, Screen.height/2+margin*4+heightTextField*3, lengthLabel, heightTextField),argumLabel4);
		GUI.Label(new Rect(margin*2+lengthTextField, Screen.height/2+margin*5+heightTextField*4, lengthLabel, heightTextField),argumLabel5);
		if (GUI.Button (new Rect (Screen.width/2+margin,Screen.height/2+margin,buttonLength,buttonHeight),"Посчитать")) {
			int argInt1=(argum1=="")?0:int.Parse(argum1);
			int argInt2=(argum2=="")?0:int.Parse(argum2);
			int argInt3=(argum3=="")?0:int.Parse(argum3);
			int argInt4=(argum4=="")?0:int.Parse(argum4);
			int argInt5=(argum5=="")?0:int.Parse(argum5);
			if ((switch10==100)||(switch10==110)||(switch10==120))
			{
				int sum = 5*argInt1+3*argInt2;
				double one = sum/20/0.9;
				result="За один:"+ one.ToString("F0")+"s, за 5 штук:"+(one*5).ToString("F0")+"s";
			}
			if (switch10==4)
			{
				argInt1 = 205;
				double sum = 205+argInt2/50+argInt3/50+argInt4/10*3+argInt5/10;
				result = "За 5 штук: "+sum.ToString("F0");
			}
			if (switch10==14)
			{
				argInt1 = 205;
				double sum = 205+argInt2/10+argInt3/50+argInt4/10*3+argInt5/10;
				result = "За 5 штук: "+sum.ToString("F0");
			}
			Debug.Log (result);
			argum1="";
			argum2="";
			argum3="";
			argum4="";
			argum5="";
			flagCount=true;
        }
		if (flagCount) {
			GUI.Label(new Rect(Screen.width/2+margin, Screen.height/2+2*margin+buttonHeight, 350, 400),result);
				}
    }
}
