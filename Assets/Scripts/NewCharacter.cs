﻿using UnityEngine;
using System.Collections;

public class NewCharacter : MonoBehaviour {
	int margin = 40, buttonHeight = 60, buttonWidth = 120, labelWidth=200,labelHeight=60,textFileldWidth=200,textFileldHeight=60;
	public string cname="",mana="10",hits="10",attack="3",defense="3";
	public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/addchar.ashx"; 
	// Use this for initialization
	void Awake () {
		Debug.Log (mainScript.chosenLogin);
		Screen.SetResolution (720, 1280, true);
	}

	IEnumerator AddCharacter(string _name,string _attack,string _defense,string _mana,string _hits)
	{
		Debug.Log (_name);
		WWWForm form = new WWWForm ();
		form.AddField ("login", mainScript.chosenUser.id);
		form.AddField ("name", _name);
		form.AddField ("attack", _attack);
		form.AddField ("defense", _defense);
		form.AddField ("mana", _mana);
		form.AddField ("hits", _hits);
		var r = new WWW (PHPUrl, form);
		yield return r;
		if (r.error != null) {
			Debug.Log ("Fail");
			Debug.Log (r.error);
		} else {
			Debug.Log ("Success");
			string result = r.text.Split ('<') [0];
			if (result [0] == 'y') {
				Application.LoadLevel ("SecondScene");
			} else 
				Debug.Log (r.text);
			
        }
    }
    // Update is called once per frame
    void OnGUI() {
        GUI.skin.textField.fontSize = 30;
		GUI.skin.label.fontSize = 30;
		GUI.Label (new Rect (Screen.width / 2 - labelWidth - margin, margin, labelWidth, labelHeight), "Имя");
		cname = GUI.TextField (new Rect (Screen.width / 2 +margin, margin, textFileldWidth, textFileldHeight),cname,25);

		GUI.Label (new Rect (margin, margin*2+labelHeight, labelWidth, labelHeight), "Атака");
		attack = GUI.TextField (new Rect (margin*2+labelWidth, margin*2+labelHeight, textFileldWidth, textFileldHeight),attack,25);
		GUI.Label (new Rect (margin, margin*3+labelHeight*2, labelWidth, labelHeight), "Защита");
		defense = GUI.TextField (new Rect (margin*2+labelWidth, margin*3+labelHeight*2, textFileldWidth, textFileldHeight),defense,25);

		
		GUI.Label (new Rect (Screen.width/2+ margin, margin*2+labelHeight, labelWidth, labelHeight), "Мана");
		mana = GUI.TextField (new Rect (Screen.width/2+margin*2+labelWidth, margin*2+labelHeight, textFileldWidth, textFileldHeight),mana,25);
		GUI.Label (new Rect (Screen.width/2 +margin, margin*3+labelHeight*2, labelWidth, labelHeight), "Хиты");
		hits = GUI.TextField (new Rect (Screen.width/2+margin*2+labelWidth, margin*3+labelHeight*2, textFileldWidth, textFileldHeight),hits,25);
	
		if (GUI.Button (new Rect (Screen.width / 2 - buttonWidth / 2, margin * 4 + labelHeight * 3, buttonWidth, buttonHeight), "Добавить")) {
			StartCoroutine (AddCharacter(cname,attack,defense,mana,hits));
        }
    }
}
