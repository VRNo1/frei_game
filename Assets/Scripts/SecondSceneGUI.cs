﻿using UnityEngine;
using System.Collections;

public class SecondSceneGUI : MonoBehaviour
{
		private TextMesh textMesh;
		//private bool newPlayer = false;
		string[] characterArray = null;
		bool charactersReturned = false, madeSplit = false;
		string characters = "";
		public GUISkin menuSkin;
		public static string chosenCharacter = "";
		public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/login2.ashx"; // адрес скрипта;
		int margin = 80, buttonHeight = 60, buttonWidth = 120, numOfReturned = 0;
		// Use this for initialization
		IEnumerator GetCharactersFromLogin (string login)
		{
				WWWForm form = new WWWForm ();
				form.AddField ("login", GameController.GetInstance().chosenUser.id);
				form.AddField ("code", "characters");
				var r = new WWW (PHPUrl, form);
				yield return r;
				Debug.Log (r.text);
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
				} else {
						if (r.text != null)
								characters = r.text;
				}
				charactersReturned = true;
		}

		void Awake ()
		{
				//Debug.Log (mainScript.chosenLogin);
				StartCoroutine (GetCharactersFromLogin (GameController.GetInstance().chosenUser.login));
				Screen.SetResolution (720, 1280, true);
				//textMesh = ((TextMesh)GameObject.FindGameObjectWithTag ("Character").GetComponent (typeof(TextMesh)));
				//textMesh.text = mainScript.chosenLogin;
				//textMesh.transform.Translate (Vector3.left * 8);
				
		}
	
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.Escape)) 
						Application.Quit (); 
				//textMesh.transform.Translate (Vector3.right * Time.deltaTime * 5);
				//textMesh.transform.position = Vector3.Lerp (textMesh.transform.position, new Vector3 (0f, 0f, 0f), 5f*Time.deltaTime);
		}

		void OnGUI ()
		{
				GUI.skin = menuSkin;
				if (charactersReturned) {
						
						if ((characters != "") && (!madeSplit)) {
								Debug.Log ("попал в ongui " + characters);
								characters = characters.Substring (0, characters.Length - 1);
								characterArray = characters.Split (';');
								numOfReturned = characterArray.Length;
						
						}
						madeSplit = true;
						for (int i=0; i<numOfReturned; i++) {
								if (GUI.Button (new Rect (Screen.width / 2 - buttonWidth / 2, margin * (i + 1) + buttonHeight * i, buttonWidth, buttonHeight), characterArray [i])) {
										chosenCharacter = characterArray [i];
										Application.LoadLevel ("FightScene");
								}
						}
						if (numOfReturned < 3) {
								if (GUI.Button (new Rect (Screen.width / 2 - buttonWidth / 2, margin * (numOfReturned + 1) + buttonHeight * numOfReturned, buttonWidth, buttonHeight), "Новый персонаж")) {
										Application.LoadLevel ("NewCharacterScene");
								}
						}
				}
		}

}
